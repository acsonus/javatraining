package chapter1.enums;

public enum CoffeeSize {

	BIG(1), HUGE(2), OVERWHELMING(3);

	private int size;

	private CoffeeSize(int size) {
		this.size = size;
	}

	public int getOunces() {
		return size;
	}
}
