package chapter1.classes;

import chapter1.enums.CoffeeSize;

public class CoffeeTest2 {
	
	private void a() {
		int _a;
		String $;
	}
	public static void main(String[] args) {
		//enum CoffeeSize { BIG, SMALL}; //it is illegal to declare inside method
		
		Coffee drink1 = new Coffee();
		drink1.size =CoffeeSize.BIG;
		
		Coffee drink2 = new Coffee();
		drink2.size  = CoffeeSize.OVERWHELMING;
		
		System.out.println(drink1.size.getOunces());
		
		for (CoffeeSize cs: CoffeeSize.values()) {
			System.out.println(cs+" "+cs.getOunces()+ " ounces");
		}
	}
}
