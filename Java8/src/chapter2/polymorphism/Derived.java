package chapter2.polymorphism;

class Apple {

	String founder = "";
	String ceo = "";

	public Apple() {
		founder = "Steeve Jobs";

	}

	void setCeo(String ceo) {

		this.ceo = ceo;
	}

	String getCeo() {
		return this.ceo;
	}

}

public class Derived extends Base {
	int x = 12;
	Apple apple;

	public Derived() {

		System.out.println("Setting x by 17");
		x = 17;

		apple = new Apple();
	}

	public static void main(String[] args) {
		Base base = new Derived();
		System.out.println("The value of x is " + base.x);
		System.out.println("casted variable " + ((Derived) base).x);
		Derived a = ((Derived) base);
		a.parametersByValue(20);
		System.out.println(a.getX());
		//a.setAppleCeo("new Bullshit");
		System.out.println(a.getAppleCeo());

	}

	public void parametersByValue(int x) {

		x = x * 10;

	}

	public void setAppleCeo(String ceoOfApple) {

		if (this.apple != null) {

			this.apple.setCeo("tim Cook");
		}

	}
	
	public String getAppleCeo() {
		if (this.apple!=null && this.apple.getCeo()!=null) {
			return this.apple.getCeo();
		}
		return null;
	}

	private String getX() {
		return String.valueOf(this.x);
	}
}