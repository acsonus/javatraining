package chapter2.polymorphism;
class GameShape{

    public void displayShape(){
        System.out.println("displaying shape");
    }
    // more code
}

class PlayerPiece extends GameShape{

    public void movePiece(){
        System.out.println("Moving game piece");
    }
    // more code 
}

class TilePiece extends GameShape{
    public void getAdgacent(){
        System.out.println("Getting adgacent files");
    }
    //more code     
}
public class TestShapes {
    public static void main(String[] args) {
        PlayerPiece shape = new PlayerPiece();
TilePiece tilePiece = new TilePiece();
        doShapes(shape);
        doShapes(tilePiece);
        // shape.displayShape();
        // shape.movePiece();
    }

    public static void doShapes(GameShape shape){
        shape.displayShape();
    }
}
