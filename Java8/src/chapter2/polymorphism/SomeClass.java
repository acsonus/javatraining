package chapter2.polymorphism;

public class SomeClass extends Bird {
	
	// static can be independent ones
	{
		System.out.print("Shit sometimes happens");
	}

	static {

		System.out.println("SomeClass static block executed");

	}

	SomeClass() {
		System.out.println("SomeClass Constructor ");
	}

	public static void main(String[] args) {
		SomeClass a = new SomeClass();

	}
}

class Bird {
	static {

		System.out.println("Bird static block executed");
	}

	Bird() {

		System.out.println("Bird constructor is called");
	}

	public void cry(String whatCry) {

		System.out.println(whatCry);
	}
}