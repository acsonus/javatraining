package chapter2.polymorphism;

public class Base {
	int x= 5;
	protected  Base() {
		System.out.println("initializing x by 7");
		x=7;
	}
}
