package chapter2.polymorphism;
class Top {
	
	public Top(String s) {
		
		System.out.println("B");
	}
}
public class Bottom2 extends Top {
	
	public Bottom2(String s) {
		super("B");
		System.out.print("C");
	}
public static void main(String[] args) {
	Top b2 =new Bottom2("this is not to be printed");
}
}
