package chapter2.polymorphism;

public class Tenor {
	public static String sing() {
		return "Tenor sings";
	}
	
	public static void main(String[] args) {
		Singer s = new Singer();
		Tenor t = new Tenor();
		System.out.println(s.sing()+" "+t.sing());
	}
}
class Singer  {
	
	public static String sing() {
		return "Singer sings";
	}
}