package tacos.data;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import tacos.TacoOrder;
public interface CassandraOrderRepository
         extends CrudRepository<TacoOrder, UUID> {
}