package tacos.data;

import org.springframework.data.repository.CrudRepository;
import tacos.Ingredient;
public interface CassandraIngredientRepository
         extends CrudRepository<Ingredient, String> {
}