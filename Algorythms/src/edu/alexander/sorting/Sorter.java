package edu.alexander.sorting;

public class Sorter {

	public void bubbleSort(int[] array) {
		boolean sorted = false;
		int iterationCounter = 0;
		while (!sorted) {
			sorted = true;
			for (int i = 0; i < array.length - 1; i++) {
				iterationCounter++;
				if (array[i] > array[i + 1]) {
					int swapNum = array[i];
					array[i] = array[i + 1];
					array[i + 1] = swapNum;
					sorted = false;
				}
			}
		}
		System.out.println("Iterations " + iterationCounter);
	}

	public void insertionSort(int[] arr) {
		int iterations = 0;
		for (int i = 0; i < arr.length; i++) {
			int current = arr[i];
			int j = i - 1;
			while (j > 0 && current < arr[j]) {
				arr[j + 1] = arr[j];
				j--;
				iterations++;
			}
			arr[j + 1] = current;
		}
		System.out.println("Iterations count "+iterations);
	}

	public void selectionSort(int[] arr) {
		for (int left = 0; left < arr.length; left++) {
			int minInd = left;
			for (int i = left; i < arr.length; i++) {
				if (arr[i] < arr[minInd]) {

				}
			}
		}
	}

	public void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			if (i != arr.length - 1)
				System.out.print(arr[i] + ",");
			else
				System.out.print(arr[i] + "\r\n");
		}
	}

	public void swap(int[] arr, int index1, int index2) {
		int temp = arr[index1];
		arr[index1] = arr[index2];
		arr[index2] = temp;
	}

	public int[] initializeArray() {
		int[] retval= { 1, 5, 2, 3, 7, 6, 12, 45, 8, 0 };
		return retval;
	}

	public static void main(String[] args) {
		
		
		Sorter sorter = new Sorter();
		int[] arr =sorter.initializeArray();
		System.out.println("Bubble sort");
		sorter.printArray(arr);
		sorter.bubbleSort(arr);
		sorter.printArray(arr);
		arr= sorter.initializeArray();
		System.out.println("Insertion Sort");
		sorter.printArray(arr);
		sorter.insertionSort(arr);
		sorter.printArray(arr);
		
		
	}
}
