package edu.aleksandr.patterns.structural.bridge;

public interface Question {
void nextQuestion();
void previousQuestion();
void newQuestion(String s);
void deleteQuestion(String s);
void displayQuestion();
void displayAllQuestions();
}
