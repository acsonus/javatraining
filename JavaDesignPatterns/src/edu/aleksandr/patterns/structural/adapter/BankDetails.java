package edu.aleksandr.patterns.structural.adapter;

public class BankDetails {
	String bankName;
	String accHolderName;
	long accNumer;

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccHolderName() {
		return accHolderName;
	}

	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}

	public long getAccNumer() {
		return accNumer;
	}

	public void setAccNumer(long accNumer) {
		this.accNumer = accNumer;
	}

	
}
