package edu.aleksandr.patterns.creational.builder;

public abstract class CD implements Packing {
public abstract String pack();
}
 