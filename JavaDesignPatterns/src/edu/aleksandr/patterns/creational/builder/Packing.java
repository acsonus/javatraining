package edu.aleksandr.patterns.creational.builder;

public interface Packing {
	public String pack();

	public int price();
}
