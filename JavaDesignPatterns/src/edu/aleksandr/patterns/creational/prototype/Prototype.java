package edu.aleksandr.patterns.creational.prototype;

interface Prototype {

	public Prototype getClone();
	
}
