package edu.aleksandr.patterns.decorator;

public interface Food {
	public String prepareFood();
	public double foodPrice();
}
