package edu.aleksandr.patterns.facade;

public class ShopKeeper {
	private MobileShop iPhone;
	private MobileShop samsung;
	private MobileShop blackberry;

	public ShopKeeper() {
		iPhone = new IPhone();
		samsung = new Samsung();
		blackberry = new Blackberry();
	}

	public void iphoneSale() {
		iPhone.modelNo();
		iPhone.price();
	}

	public void samsungSale() {
		samsung.modelNo();
		samsung.price();
	}

	public void blackberrySale() {
		blackberry.modelNo();
		blackberry.price();
	}
}