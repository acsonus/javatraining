package edu.aleksandr.pattrns.creational.abstractfactory;

public interface Bank {
	
	String getBankName();

}
